import java.util.ArrayList;
/**
 * Beschreiben Sie hier die Klasse Array.
 * 
 * @author Frank Schiebel 
 * @version 0.1
 */
public class Array
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    private int length;
    private int maxvalue = 8;
    private ArrayList<Integer> theArray = new ArrayList<>();

    /**
     * Konstruktor für Objekte der Klasse Array
     */
    public Array(int laenge)
    {
        length = laenge;
        for(int i=0; i<length; i++) {
           theArray.add((int) (Math.random() * maxvalue) + 1); 
        }
    }

    /**
     * Berechnet die Summe aller Array-Elemente iterativ
     * 
     * @return        die Summe aller Arrayelemente
     */
    public int sumIterativ()
    {
        // tragen Sie hier den Code ein
        return 0;
    }
    
     /**
     * Berechnet die Summe aller Array-Elemente rekursiv
     * 
     * @return        die Summe aller Arrayelemente
     */
    public int sumRekursiv()
    {
        // tragen Sie hier den Code ein
        return 0;
    }
}
